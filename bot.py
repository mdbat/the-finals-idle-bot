import random

import keyinput, time, win32gui, win32api, win32con, win32process, cv2, sys, ctypes
from PIL import ImageGrab
import numpy as np
import os, signal, threading
import re
import datetime
import signal
import argparse as ap


# Exit on interrupt (this is a multithreaded program so do it ourselves)
def interrupt_handler(signum, frame):
    os._exit(1) # Kinda hacky but I couldnt care less about a clean exit


signal.signal(signal.SIGINT, interrupt_handler)


# Position of THE FINALS window
window_x = -1
window_y = -1
window_width = -1
window_height = -1

# Windows process related stuff
window_pid = -1
window_path = ""
window_found = False

# Whether this is targetting the steam version of the game,
# known through the use of the "steam" launch options
isSteamVersion = False

# Launch options parsed as arguments in the "optionname" or "optioname=value" format
launch_options = {}


def log(msg: str):
    now = datetime.datetime.now()
    print(f'[{now.hour:{"0"}{2}}:{now.minute:{"0"}{2}}:{now.second:{"0"}{2}}] {msg}')


# Routine to kill a process, used when it gets frozen permanently
def killprocess(pid):
    try:
        os.kill(pid, signal.SIGTERM)
    except:
        print('Failed to kill the process, will try again later.')


# Callback for windows enumeration
def get_thefinals_window(hwnd, extra):
    if win32gui.GetWindowText(hwnd) != "THE FINALS  ":
        return

    global window_found
    window_found = True

    global window_pid
    global window_path
    thread_id, window_pid = win32process.GetWindowThreadProcessId(hwnd)

    try:
        handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, window_pid)
    except:
        print('Failed to open process')
        return

    window_path = win32process.GetModuleFileNameEx(handle, 0)

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))
    global window_x
    global window_y
    global window_width
    global window_height
    window_x = window_pos[0]
    window_y = window_pos[1]
    window_width = rect[2]
    window_height = rect[3]


# Keys to press to move around
keys = [keyinput.W, keyinput.D, keyinput.S, keyinput.A]

# When moving around, how long in seconds since the last keypress.
last_keypress_timestamp = time.time()

# Start with a 'random' duration of activity 30 seconds
random_duration = 30


# Function to move around ingame
def move_around():
    current_time = time.time()

    global last_keypress_timestamp
    global random_duration

    if (current_time - last_keypress_timestamp) > random_duration:
        rand_key_index = random.randint(0, 3)

        keyinput.holdKey(keys[rand_key_index], 0.2)

        last_keypress_timestamp = time.time()

        # Re-generate a random number between 30 and 60. If the amount of seconds since last keypress is greater,
        # We press a random key for two tenth of a second.
        random_duration = random.uniform(30.0, 60.0)


# Function to press ESC
def press_esc(x, y):
    keyinput.pressKey(keyinput.ESC)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.ESC)


# Function to press space
def press_space(x, y):
    keyinput.pressKey(keyinput.SPACE)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.SPACE)


# Function to kill the game when an engine error pops up
def kill_game_for_error(x, y):
    log('Killing the game after having detected an error')
    killprocess(window_pid)


def kill_game_for_ready_timeout():
    log('Killing the game because we did not ready up for an hour')
    global last_ready_up
    last_ready_up = time.time()
    killprocess(window_pid)


last_ready_up = time.time()


def ready_up(x, y):
    global last_ready_up
    last_ready_up = time.time()
    keyinput.click(x, y)


def hold_ctrl(x, y):
    keyinput.holdKey(keyinput.CTRL, 2.0)


def press_e_and_aim_down(x, y):
    keyinput.pressKey(keyinput.E)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.E)

    # Aim down for about three seconds
    for i in range(300):
        # This is window-specific
        keyinput.move_directinput(0, 300)
        time.sleep(0.01)


def launch_at_random_time(x, y):
    duration = random.uniform(0.0, 20.0)

    time.sleep(duration)

    keyinput.pressKey(keyinput.E)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.E)


def wait_for_second_player(x, y):
    log("Waiting for second player...")
    time.sleep(60.0)


def wait_for_third_player(x, y):
    log("Waiting for third player...")
    time.sleep(60.0)


# Find a certain item in an image, returns the estimated position and associated threshold
def find_item(img, template):
    # Apply template Matching
    res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    w, h = template.shape[::-1]

    # Top left of the area
    top_left = max_loc
    # Buttom right of the area
    bottom_right = (top_left[0] + w, top_left[1] + h)

    # Zone to click if we do need
    middle_x = (top_left[0] + bottom_right[0]) / 2
    middle_y = (top_left[1] + bottom_right[1]) / 2

    return {'threshold': max_val, 'x': middle_x, 'y': middle_y, 'raw_result': res}


# Take a screenshot of the window
def screenshot(x, y, width, height):
    image = ImageGrab.grab(bbox=(x, y, x + width, y + height))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image


# The previous image used in the watchdog
old_img = None


# Whether we should kill the game when it appears frozen
kill_if_frozen = True


# Whether we should kill the game if we haven't been readying up for a long time
kill_if_ready_timeout = True


# Watchdog running on another thread, makes sure the THE FINALS process didn't crash or is not stucked
def process_watchdog():
    while True:
        # Check for the process being alive every 5 minutes
        time.sleep(300)

        global window_pid
        global window_path
        global window_found

        # Try to find the window handle
        hwnd = win32gui.FindWindow(None, "THE FINALS  ")

        # Make sure the Window is still alive
        if hwnd == 0:
            window_found = False
            # Restart the process if the window died
            log('Game is not on, re-starting it...')

            os.system("start \"\" steam://rungameid/2073850")

            continue

        # Refresh the window's position
        win32gui.EnumWindows(get_thefinals_window, None)

        # Put it in foreground
        try:
            win32gui.SetForegroundWindow(hwnd)
        except:
            print('Failed to put the window in foreground')

        global kill_if_frozen
        global old_img

        if kill_if_frozen:
            # Capture the window in color
            color_img = screenshot(window_x, window_y, window_width, window_height)

            # Convert it to grayscale for faster processing
            img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

            if old_img is not None:
                # Compare the current image to the old one,
                # if it didn't change within 60 seconds, the process is likely stuck
                result = find_item(img, old_img)

                # We deem the image as being the same if it matches over 99.999%
                if result['threshold'] > 0.99999:
                    log('Process appears to be frozen, killing it for a later restart')
                    # Kill the process if it matches
                    killprocess(window_pid)

            # Save the current image to compare it to the next one being read
            old_img = img


# Templates that we are looking for in the image
play_button = cv2.imread("Images/play_button.png", cv2.IMREAD_GRAYSCALE)
play_button_hovered = cv2.imread("Images/play_button_hovered.png", cv2.IMREAD_GRAYSCALE)
quickplay_button = cv2.imread("Images/quickplay_button.png", cv2.IMREAD_GRAYSCALE)
quickplay_button_hovered = cv2.imread("Images/quickplay_button_hovered.png", cv2.IMREAD_GRAYSCALE)
respawn_button = cv2.imread("Images/respawn_button.png", cv2.IMREAD_GRAYSCALE)
skip_button_big = cv2.imread("Images/skip_button_big.png", cv2.IMREAD_GRAYSCALE)
skip_button_small = cv2.imread("Images/skip_button_small.png", cv2.IMREAD_GRAYSCALE)
main_menu_button = cv2.imread("Images/main_menu_button.png", cv2.IMREAD_GRAYSCALE)
ok_button = cv2.imread("Images/ok_button.png", cv2.IMREAD_GRAYSCALE)
ok_button_hovered = cv2.imread("Images/ok_button_hovered.png", cv2.IMREAD_GRAYSCALE)

# Specific modes
quick_cash_button = cv2.imread("Images/quick_cash_button.png", cv2.IMREAD_GRAYSCALE)
quick_cash_button_button_hovered = cv2.imread("Images/quick_cash_button_hovered.png", cv2.IMREAD_GRAYSCALE)

bank_it_button = cv2.imread("Images/bank_it_button.png", cv2.IMREAD_GRAYSCALE)
bank_it_button_hovered = cv2.imread("Images/bank_it_button_hovered.png", cv2.IMREAD_GRAYSCALE)

solo_bank_it_button = cv2.imread("Images/solo_bank_it_button.png", cv2.IMREAD_GRAYSCALE)
solo_bank_it_button_hovered = cv2.imread("Images/solo_bank_it_button_hovered.png", cv2.IMREAD_GRAYSCALE)

UI_elements = [{'image': respawn_button, 'threshold': 0.90, 'callback': press_space},
               {'image': play_button, 'threshold': 0.95, 'callback': ready_up},
               {'image': play_button_hovered, 'threshold': 0.95, 'callback': ready_up},
               {'image': quickplay_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': quickplay_button_hovered, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': play_button_hovered, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': play_button_hovered, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': skip_button_big, 'threshold': 0.95, 'callback': press_esc},
               {'image': skip_button_small, 'threshold': 0.95, 'callback': press_esc},
               {'image': main_menu_button, 'threshold': 0.95, 'callback': press_esc},
               {'image': ok_button, 'threshold': 0.95, 'callback': keyinput.click},
               {'image': ok_button_hovered, 'threshold': 0.95, 'callback': keyinput.click}]

# Set ourselves as DPI aware, or else we won't get proper pixel coordinates if scaling is not 100%
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)

parser = ap.ArgumentParser(description='Parse warnings from a Ninja-Build output.')

parser.add_argument('--mode', dest='mode', type=str,
                        default=None,
                        help="Specify the mode you wish to queue on (e.g. 'bank-it')")

parser.add_argument('--no-watchdog', dest='no_watchdog', action="store_true",
                        help="Disable the watchdog (if you find it too agressive to detect freezes etc.)")

args = parser.parse_args()

if args.mode is not None:
    if args.mode == 'quick-cash':
        print('Selected Quick Cash mode')
        UI_elements.append({'image': quick_cash_button, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.append({'image': quick_cash_button_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    elif args.mode == 'bank-it':
        print('Selected Bank It mode')
        UI_elements.append({'image': bank_it_button, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.append({'image': bank_it_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    elif args.mode == 'solo-bank-it':
        print('Selected Solo Bank It mode')
        UI_elements.append({'image': solo_bank_it_button, 'threshold': 0.95, 'callback': keyinput.click})
        UI_elements.append({'image': solo_bank_it_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})
    else:
        print(f'Unknown game mode: {args.mode}')
        sys.exit(1)
else:
    print('Defaulting to Solo Bank It mode')
    UI_elements.append({'image': solo_bank_it_button, 'threshold': 0.95, 'callback': keyinput.click})
    UI_elements.append({'image': solo_bank_it_button_hovered, 'threshold': 0.95, 'callback': keyinput.click})

log("THE FINALS bot starting in 5 seconds, bring THE FINALS window in focus...")
time.sleep(5)

win32gui.EnumWindows(get_thefinals_window, None)

# If the window hasn't been found, exit
if not window_found:
    log("No THE FINALS window found")
    sys.exit(1)

# Start the watchdog thread
if not args.no_watchdog:
    watchdogthread = threading.Thread(target=process_watchdog)
    watchdogthread.start()
else:
    print('Watchdog disabled')

while True:
    # If the window died, stop running analysis for a moment */
    if window_found is False:
        time.sleep(5)
        continue

    # Capture the window in color
    color_img = screenshot(window_x, window_y, window_width, window_height)
    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

    found_UI_element = False

    # Try to find any of the templates
    for element in UI_elements:
        result = find_item(img, element['image'])

        if result['threshold'] > element['threshold']:
            found_UI_element = True

            # Click all elements that potentially match
            if 'multiple' in element:
                num_of_points = int(element['multiple'])
                raw_result = result['raw_result']

                # Find the highest matching patterns
                coords_flattened = np.argpartition(raw_result.flatten(), -num_of_points)[-num_of_points:]
                coords_2d_yx = np.unravel_index(coords_flattened, raw_result.shape)

                coords = []

                w, h = element['image'].shape[::-1]

                for y, x in zip(coords_2d_yx[0], coords_2d_yx[1]):
                    coords.append((x + w / 2, y + h / 2))

                # Click them all with a delay of half a second inbetween
                for coord in coords:
                    element['callback'](window_x + int(coord[0]), window_y + int(coord[1]))
                    time.sleep(0.5)
            else:
                element['callback'](window_x + int(result['x']), window_y + int(result['y']))
            break

    # If no UI element got found, move around is the default behavior
    if found_UI_element is False:
        move_around()

    # Sleep for a second
    time.sleep(1.0)

    if kill_if_ready_timeout:
        # In case we haven't been readying up for an hour, kill the game
        current_time = time.time()

        if current_time - last_ready_up > 3600:
            kill_game_for_ready_timeout()
