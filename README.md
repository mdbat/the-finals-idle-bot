# THE FINALS Idle bot
This program is designed for farming XP on THE FINALS by continuously queuing 
for matches and moving around in game to avoid kicks due to inactivity.  
The bot accounts for any UI prompt, error message, etc. and can also select a
specific character on the character selection screen.

## Setup
* Install Python 3 from https://www.python.org/downloads/. Check the option
"Add to PATH" to be able to launch python from the commandline.
* Install the packages required by the program using PIP with the command
"pip install ..." (or "pip3 install..."). The required packages are:
    * pypiwin32 : for access to the Windows API (windowing, input, ...).
    * pillow : used to take screenshots of the desktop.
    * opencv-python : used for image analysis.
* Download this program (duh !).

## How to run
* Start THE FINALS in 1024x768 resolution and windowed mode, and simply run the program 
using the command "python bot.py" (or "python3 bot.py"). The bot should start its
keypresses 5 seconds after launch.  
* Launch options:
Available launch options are:
    * '--mode=XXX'. If you want the bot to select a particular mode, add the name of the mode you desire
    as a parameter. Can either be 'quick-cash', 'bank-it', 'duos' or 'solo-bank-it'